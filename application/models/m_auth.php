<?php

class M_auth extends CI_model 
{
    public function getAllUser()
    {
        $query = $this->db->get('user');
        return $query->result_array();

        // // Cara lain mengambil tabel mahasiswa dan
        // // menampilkan result berupa array di CI
        // return $this->db->get('mahasiswa')->result_array();
    }
    public function createUser()
    {
        $data = [
            'name'  => htmlspecialchars($this->input->post('name',true)),
            'email' => htmlspecialchars($this->input->post('email',true)),
            'image' => 'default.png',
            'password' => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
            'role_id' => 2,
            'is_active' => 1,
            'date_created'=> time()
        ];

        $this->db->insert('user', $data);
    }

    function _login(){
        $email = $this->input->post('email');
        $password = $this->input->post('password');

        $user = $this->db->get_where('user',['email'=>$email])->row_array();

        // jika email ada dalam database
        if ($user){
            // jika email sudah diverifikasi / sudah aktifasi
            if ($user['is_active']==1){
                // cek password dan email
                // jika password dan email benar
                if(password_verify($password,$user['password'])){
                    $data = [
                        'email' => $user['email'],
                        'role_id' => $user['role_id']
                    ];
                    $this->session->set_flashdata('flash', ' Login ');
                    $this->session->set_userdata($data);
                    redirect('User');        
                }
                // jika password dan email salah
                else{
                    $this->session->set_flashdata('flash',' password and email do not match ');
                    redirect('auth');
                }
            }
            // jika email belum diverifikasi / belum aktifasi
            else{
                $this->session->set_flashdata('flash',' This email has not been activated');
                redirect('auth');
            }
            $this->session->set_flashdata('flash', ' Login ');
            redirect('auth');    
        }
        // jika email tidak ada dalam database
        else {
            $this->session->set_flashdata('flash','Email is not registered!');
            // $this->session->set_flashdata('flash',
            // '<div class="alert alert-danger" role="alert">Email is not registered!</div>');
            redirect('auth');
        }
        return $user;
    }

    public function getUserSession()
    {
        $user = $this->db->get_where('user',['email'=>$this->session->userdata('email')])->row_array();
        return $user;
    }

    // public function hapusDataMahasiswa($id)
    // {
    //     $this->db->where('id', $id);
    //     $this->db->delete('mahasiswa');

    //     // cara penulisan CI lain
    //     // $this->db->delete('mahasiswa',['id'=>$id]);
    // }

    // public function getUserByEmail($email)
    // {
    //     return $this->db->get_where('user',['email'=>$email])->row_array();
    // }

    // public function ubahDataMahasiswa()
    // {
    //     $data = [
    //         "nama" =>$this->input->post('nama',true),
    //         "nim" =>$this->input->post('nim',true),
    //         "email" =>$this->input->post('email',true),
    //         "jurusan" =>$this->input->post('jurusan',true)
    //     ];

    //     $this->db->where('id',$this->input->post('id'));
    //     $this->db->update('mahasiswa', $data);
    // }

    // public function cariDataMahasiswa()
    // {
    //     $keyword = $this->input->post('keyword', true);
    //     $this->db->like('nama',$keyword);
    //     $this->db->or_like('nim',$keyword);
    //     $this->db->or_like('email',$keyword);
    //     $this->db->or_like('jurusan',$keyword);
    //     return $this->db->get('mahasiswa')->result_array();
    // }
}