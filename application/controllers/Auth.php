<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('m_auth');
    }
    // index / login page
    public function index()
    {
        $this->form_validation->set_rules('email','Email','required|trim|valid_email');
        $this->form_validation->set_rules('password','Password','required|trim');
        // jiak tidak lolos form validasi
        if ($this->form_validation->run()== false){
            $data['title'] = 'Login Page';
            $this->load->view('templates/auth_header', $data);
            $this->load->view('auth/login');
            $this->load->view('templates/auth_footer');
        }
        else {
            $this->m_auth->_login();
        }
    }

    public function registration()
    {
        $this->form_validation->set_rules('name','Name','required|trim');
        $this->form_validation->set_rules('email','Email','required|trim|valid_email|is_unique[user.email]',[
            'is_unique' => 'This email has already registered!'
        ]);
        $this->form_validation->set_rules('password1','Password','required|trim|min_length[3]|matches[password2]',[
            'matches' => 'Password dont match!',
            'min_length' => 'Password min 3 char!'
        ]);
        $this->form_validation->set_rules('password2','Password','required|trim|min_length[3]|matches[password1]');

        if ($this->form_validation->run()==false)
        {
            $data['title'] = 'Registration Page';
            $this->load->view('templates/auth_header', $data);
            $this->load->view('auth/registration');
            $this->load->view('templates/auth_footer');
        }
        else {
            $this->m_auth->createUser();
            $this->session->set_flashdata('flashBS',
            '<div class="alert alert-success" role="alert">
                Congratulation! your account has been created.<br>You can verified your email
            </div>');
            redirect('auth');
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('role_id');

        $this->session->set_flashdata('flashBS','<div class="alert alert-success" role="alert">You have been logged out!</div>');
        redirect('auth');
    }
}
