<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller 
{	
    public function __construct() 
    {
        parent::__construct();        
        $this->load->library('form_validation');
        $this->load->model('m_auth');
    }
    
    public function index()
    {
        $data['title'] = 'My Profile';
        // if($this->session->flashdata('flash')):
        //     $this->session->flashdata('flash') ;
        // endif;  
        $data['user']=$this->m_auth->getUserSession();
        $this->load->view('templates/user_header',$data);
        $this->load->view('templates/user_sidebar',$data);
        $this->load->view('templates/user_topbar',$data);
        $this->load->view('user/index',$data);
        $this->load->view('templates/user_footer');

    }
}
