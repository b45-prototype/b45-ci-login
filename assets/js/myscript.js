// buat fariabel flasshData yang isinya
// element flash-data / class flash-data dari html
// yang isisnya dari element data('flashdata') / data-flashdata html
const flashData = $('.flash-data').data('flashdata');

// jika flashdata ada isinya
if (flashData) 
{
    Swal.fire
    ({
        title: 'Your Account',
        type: 'success',
        text: 'Has Been' + flashData
        
    });
}

// jika class tombol-hapus di klik
$('.tombol-hapus').on('click', function(e){
    // menghentikan aksi default / menghentikan aksi href
    e.preventDefault();
    // mengambil value href pada tombol hapus yang dipilih
    const href = $(this).attr('href');
    Swal.fire
    ({
        title: 'Apakah anda yakin?',
        text: "data mahasiswa akan dihapus",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya hapus'
        // aksi dari flash sweetalert
    }).then((result) => 
        {
            // jika ditekan tombol Ya hapus
        if (result.value) 
        {
            // jalankan kembali aksi href
            document.location.href = href;
        }
        })
});